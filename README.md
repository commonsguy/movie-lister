This sample app illustrates how to query the `MediaStore` for movie titles.
It also shows how to insert content into the `MediaStore`, both for Android 10
and for older devices.

This sample app is in support of [this blog post](https://commonsware.com/blog/2019/12/29/scoped-storage-stories-reading-mediastore.html)
by Mark Murphy of [CommonsWare](https://commonsware.com).