/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.
*/

package com.commonsware.movielister

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch

sealed class MainViewState {
  object Loading : MainViewState()
  data class Content(val videos: List<VideoModel>) : MainViewState()
  object Error : MainViewState()
}

class MainMotor(private val repo: VideoRepository) : ViewModel() {
  private val _states = MutableLiveData<MainViewState>()
  val states: LiveData<MainViewState> = _states
  val securityExceptions = Channel<SecurityException>()
  var lastSelectedVideoId: Long = -1

  fun copyAsset() {
    viewModelScope.launch(Dispatchers.Main) {
      try {
        repo.copyAsset("test.mp4")
        refreshImpl()
      } catch (t: Throwable) {
        Log.e("MovieLister", "Exception in copying asset", t)
        _states.value = MainViewState.Error
      }
    }
  }

  fun refresh() {
    _states.value = MainViewState.Loading

    viewModelScope.launch(Dispatchers.Main) {
      try {
        refreshImpl()
      } catch (t: Throwable) {
        Log.e("MovieLister", "Exception in loading videos", t)
        _states.value = MainViewState.Error
      }
    }
  }

  fun setTags(id: Long, tags: String, description: String) {
    lastSelectedVideoId = id

    viewModelScope.launch(Dispatchers.Main) {
      try {
        repo.setTagsAndDescription(id, tags, description)
        refreshImpl()
      } catch (sec: SecurityException) {
        securityExceptions.offer(sec)
      }
    }
  }

  private suspend fun refreshImpl() {
    _states.value = MainViewState.Content(repo.listVideos() ?: listOf())
  }
}