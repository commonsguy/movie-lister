package com.commonsware.movielister

data class VideoModel(
  val id: Long,
  val title: String?,
  val tags: String?,
  val description: String?
)