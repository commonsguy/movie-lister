/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.
*/

package com.commonsware.movielister

import android.Manifest
import android.app.Activity
import android.app.RecoverableSecurityException
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import androidx.recyclerview.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

private const val PERMS_READ = 1337
private const val PERMS_WRITE = 1338
private const val REQUEST_RSE = 1339
private const val PERMS_WRITE_TAGS = 1340

class MainActivity : AppCompatActivity() {
  private val motor: MainMotor by viewModel()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    LinearLayoutManager(this).let {
      titles.layoutManager = it
      titles.addItemDecoration(DividerItemDecoration(this, it.orientation))
    }

    val adapter = TitleAdapter(layoutInflater, ::setTagsAndDescription)

    titles.adapter = adapter

    motor.states.observe(this) { state ->
      when (state) {
        MainViewState.Loading -> {
          loading.visibility = View.VISIBLE
          adapter.submitList(listOf())
        }
        is MainViewState.Content -> {
          loading.visibility = View.GONE
          adapter.submitList(state.videos)
        }
        MainViewState.Error -> {
          loading.visibility = View.GONE
          adapter.submitList(listOf())
          Toast.makeText(this, R.string.error, Toast.LENGTH_LONG).show()
        }
      }
    }

    lifecycleScope.launch {
      for (sec in motor.securityExceptions) {
        handleSecurityException(sec)
      }
    }

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
      motor.refresh()
    } else {
      loadAll()
    }
  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.actions, menu)

    return super.onCreateOptionsMenu(menu)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.copy -> copyAsset()
      R.id.refresh -> motor.refresh()
      R.id.all -> loadAll()
      else -> return super.onOptionsItemSelected(item)
    }

    return true
  }

  override fun onRequestPermissionsResult(
    requestCode: Int,
    permissions: Array<out String>,
    grantResults: IntArray
  ) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)

    when (requestCode) {
      PERMS_READ -> {
        if (grantResults.firstOrNull() == PackageManager.PERMISSION_GRANTED) {
          motor.refresh()
        }
      }
      PERMS_WRITE -> {
        if (grantResults.firstOrNull() == PackageManager.PERMISSION_GRANTED) {
          motor.copyAsset()
        }
      }
      PERMS_WRITE_TAGS -> {
        setTagsAndDescription(motor.lastSelectedVideoId)
      }
    }
  }

  override fun onActivityResult(
    requestCode: Int,
    resultCode: Int,
    data: Intent?
  ) {
    if (requestCode == REQUEST_RSE) {
      if (resultCode == Activity.RESULT_OK) {
        val id = motor.lastSelectedVideoId

        motor.setTags(id, "ID #$id", "good!")
      }
    } else {
      super.onActivityResult(requestCode, resultCode, data)
    }
  }

  private fun loadAll() {
    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
      motor.refresh()
    } else {
      requestPermissions(
        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
        PERMS_READ
      )
    }
  }

  private fun copyAsset() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q ||
      checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    ) {
      motor.copyAsset()
    } else {
      requestPermissions(
        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
        PERMS_WRITE
      )
    }
  }

  private fun setTagsAndDescription(id: Long) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q ||
      checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    ) {
      motor.setTags(id, "ID #$id", "good!")
    } else {
      motor.lastSelectedVideoId = id

      requestPermissions(
        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
        PERMS_WRITE_TAGS
      )
    }
  }

  private fun handleSecurityException(sec: SecurityException) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
      if (sec is RecoverableSecurityException) {
        val intentSender = sec.userAction.actionIntent.intentSender

        try {
          startIntentSenderForResult(
            intentSender,
            REQUEST_RSE,
            null,
            0,
            0,
            0,
            null
          )
        } catch (e: SendIntentException) {
          Log.e("TestApp", "Exception launching dialog", e)
        }
      } else {
        Log.e("MovieLister", "Unexpected exception", sec)
      }
    } else {
      Log.e("MovieLister", "Unexpected exception", sec)
    }
  }

  class TitleAdapter(
    private val inflater: LayoutInflater,
    private val onClick: (Long) -> Unit
  ) :
    ListAdapter<VideoModel, RowHolder>(VideoDiffer) {
    override fun onCreateViewHolder(
      parent: ViewGroup,
      viewType: Int
    ) = RowHolder(
      inflater.inflate(
        android.R.layout.simple_list_item_2,
        parent,
        false
      ), onClick
    )

    override fun onBindViewHolder(holder: RowHolder, position: Int) {
      holder.bind(getItem(position))
    }
  }

  class RowHolder(root: View, private val onClick: (Long) -> Unit) :
    RecyclerView.ViewHolder(root) {
    private val title = root.findViewById<TextView>(android.R.id.text1)
    private val description = root.findViewById<TextView>(android.R.id.text2)

    fun bind(video: VideoModel) {
      title.text = video.title
      description.text = "description: ${video.description}, tags: ${video.tags}"
      itemView.setOnClickListener { onClick(video.id) }
    }
  }

  private object VideoDiffer : DiffUtil.ItemCallback<VideoModel>() {
    override fun areItemsTheSame(oldItem: VideoModel, newItem: VideoModel) =
      oldItem === newItem

    override fun areContentsTheSame(oldItem: VideoModel, newItem: VideoModel) =
      oldItem.title == newItem.title && oldItem.tags == newItem.tags
  }
}
