/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.
*/

package com.commonsware.movielister

import android.annotation.TargetApi
import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.media.MediaScannerConnection
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream

private val PROJECTION =
  arrayOf(
    MediaStore.Video.Media._ID,
    MediaStore.Video.Media.TITLE,
    MediaStore.Video.Media.TAGS,
    MediaStore.Video.Media.DESCRIPTION
  )
private const val SORT_ORDER = MediaStore.Video.Media.TITLE

class VideoRepository(private val context: Context) {
  private val collection =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
      MediaStore.Video.Media.getContentUri(
        MediaStore.VOLUME_EXTERNAL
      )
    } else {
      MediaStore.Video.Media.EXTERNAL_CONTENT_URI
    }

  suspend fun copyAsset(filename: String) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
      copyAssetQ(filename)
    } else {
      copyAssetLegacy(filename)
    }
  }

  suspend fun listVideos(): List<VideoModel>? =
    withContext(Dispatchers.IO) {
      val resolver = context.contentResolver

      resolver.query(collection, PROJECTION, null, null, SORT_ORDER)
        ?.use { cursor ->
          cursor.mapToList {
            VideoModel(
              it.getLong(0),
              it.getString(1),
              it.getString(2),
              it.getString(3)
            )
          }
        }
    }

  suspend fun setTagsAndDescription(id: Long, tags: String, description: String) =
    setMetadata(id, ContentValues().apply {
      put(MediaStore.Video.Media.TAGS, tags)
      put(MediaStore.Video.Media.DESCRIPTION, description)
    })

  private suspend fun setMetadata(id: Long, values: ContentValues) =
    withContext(Dispatchers.IO) {
      val uri = ContentUris.withAppendedId(collection, id)

      if (Build.VERSION.SDK_INT >= 29) {
        val tempValues = ContentValues().apply {
          put(MediaStore.Video.Media.IS_PENDING, 1)
        }

        context.contentResolver.update(uri, tempValues, null, null)
      }

      val updatedValues =
        if (Build.VERSION.SDK_INT >= 29) {
          ContentValues(values).apply {
            put(MediaStore.Video.Media.IS_PENDING, 0)
          }
        } else {
          values
        }

      context.contentResolver.update(uri, updatedValues, null, null)
    }

  @TargetApi(Build.VERSION_CODES.Q)
  private suspend fun copyAssetQ(filename: String) =
    withContext(Dispatchers.IO) {
      val values = ContentValues().apply {
        put(MediaStore.Video.Media.DISPLAY_NAME, filename)
        put(MediaStore.Video.Media.RELATIVE_PATH, "Movies")
        put(MediaStore.Video.Media.MIME_TYPE, "video/mp4")
        put(MediaStore.Video.Media.IS_PENDING, 1)
      }

      val resolver = context.contentResolver
      val uri = resolver.insert(collection, values)

      uri?.let {
        resolver.openOutputStream(uri)?.use { outputStream ->
          context.assets.open(filename).use { it.copyTo(outputStream) }
        }

        values.clear()
        values.put(MediaStore.Video.Media.IS_PENDING, 0)
        resolver.update(uri, values, null, null)
      } ?: throw RuntimeException("MediaStore failed for some reason")
    }

  private suspend fun copyAssetLegacy(filename: String) =
    withContext(Dispatchers.IO) {
      val file = File(
        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES),
        filename
      )

      FileOutputStream(file).use { outputStream ->
        context.assets.open(filename).use { it.copyTo(outputStream) }
      }

      MediaScannerConnection.scanFile(
        context,
        arrayOf(file.absolutePath),
        arrayOf("video/mp4"),
        null
      )
    }

  private fun <T : Any> Cursor.mapToList(predicate: (Cursor) -> T): List<T> =
    generateSequence { if (moveToNext()) predicate(this) else null }
      .toList()
}